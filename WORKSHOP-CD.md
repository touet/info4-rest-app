# Kubernetes

The plan is to try to deploy it on a Kubernetes cluster and run the app as a micro-service application.

Let's play with kubernetes a bit.
First create a local kubernetes environment with `minikube`.  
It will create a local cloud.

```
minikube start
```

We have the possibilty to use the same kind of command than with docker but it is a bit complicated.  
To me the simplest is to use a yaml file to describe the deployments and the different stuffs that we want to do inside the cluster (yes, just like a `docker-compose.yml` file would do).

Once minikube VM is started, start using the basic kubernetes commands, like:

```
kubectl get pods
```

You clearly need to understand the basic between a pod, a service, a node and a deployment.  
Here, we will manage *deployment* that are responsible of a dedicated microservice. This *deployment* will launch several replicas of docker containers on pods.  
Those services will be able to connect to each other thanks to *services*.  
These concepts are described in the `kubernetes.yml` file.

You first need to tell `minikube` that you are relying on local docker images to deploy container onto its cloud.  
As minikube is also a container. It has basically no knowledge of your docker instances.  
Execute the following command for your local docker to deploy images to the minikube environment:

```
eval $(minikube -p minikube docker-env)
```

Now, build the two projects *rest-app* project (the one in this repository) and the *static-web* project.  
Build the docker images with `docker build -t <image-name> .`.  
Name the images `k8s-rest-app` and `k8s-nginx` respectively.

It can happen that minikube still have trouble finding the images, so you may need to execute `minikube image load <image-name>` to have it loaded in minikube environment.

The `kubernetes.yml` contains the description of all the services that needs to be deployed for the microservices app to work correctly.  
The *selectors* allows the *services* to know which deployment it needs to select to apply the port forward onto.

Execute `kubectl apply -f kubernetes.yml` to deploy this application in the minikube cloud.

Execute the following command to see the pods that are being run by kubernetes in the current cloud:
```
kubectl get pods
```

It should show 2 pods of each deployment.

Run `kubectl get services` to list the network configuration needed for the project.

If everything is marked as `Running`, it means the app is ready.

Run `kubectl logs rest-app-<deploymentId>-<podId>` to see if the app is started correctly.  
If that is the case, you need to tell minikube to open a gateway to the exposed `my-nginx` service with:
```
minikube service my-nginx
```

It opens a browser tab to the app hosted in the cloud.  
The db servers are empty with data but you can send POST requests to the same URL on the */guest*  path to add new guest.
Reload the browser tab to see the guest now being displayed.

Have a look at the different pods to see the path that the request made through the pods.

### other useful commands
```
k apply -f kubernetes.yaml
k describe service my-nginx
minikube service my-nginx
k exec nginx-deployment-55c968d875-pgwms  -- curl http://localhost:9090
```

