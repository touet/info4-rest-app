# Spring Boot Rest Application

This project contains the java part of a multi-tier project to be deployed on a Kubernetes cluster.

The Gitlab CI is used to compile the project and to deploy it onto a cloud provider.

## Build

use gradle or the embedded `gradlew` file to build the project.

Generate the jar

```
gradle build
```

or 

```
./gradlew build
```
